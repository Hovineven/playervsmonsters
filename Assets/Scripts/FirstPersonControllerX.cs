﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonControllerX : MonoBehaviour
{
    private Vector2 PrevMpos;

    private void Start()
    {
        PrevMpos = Input.mousePosition;
    }

    private void FixedUpdate()
    {
        transform.Rotate(PrevMpos.y - Input.mousePosition.y, 0, 0);
        PrevMpos = Input.mousePosition;
    }
}
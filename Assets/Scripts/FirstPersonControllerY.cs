﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonControllerY : MonoBehaviour
{
    private Vector2 PrevMpos;

    private void Start()
    {
        PrevMpos = Input.mousePosition;
    }

    private void FixedUpdate()
    {
        transform.Rotate(0, Input.mousePosition.x - PrevMpos.x, 0);
        PrevMpos = Input.mousePosition;
    }
}
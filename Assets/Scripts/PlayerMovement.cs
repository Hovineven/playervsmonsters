﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float Slowness;

    private void Start()
    {
        Slowness = 3;
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-1/ Slowness, 0, 0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(1/ Slowness, 0, 0);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(0,0,-1/ Slowness);
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(0,0,1/ Slowness);
        }
    }
}